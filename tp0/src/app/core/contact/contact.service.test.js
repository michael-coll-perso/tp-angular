'use strict';

describe('Contact service', function () {
    var $scope;

    beforeEach(inject.strictDi());
    beforeEach(module('zenContact.core.contact'));

    it('should contains 8 contacts', inject(function ($controller) {
        expect($scope.nameFilter).toBeUndefined();

        $controller('ContactListController', {
            $scope: $scope
        });

        expect(angular.isFunction($scope.nameFilter)).toBeTruthy();
    }));

});
