angular.module('zenContact.sections', ['ngRoute',
    'zenContact.sections.list', 'zenContact.sections.edit', 'zenContact.sections.nav'])
.config(function($routeProvider) {
    'use strict';
    $routeProvider.when('/list', {
        templateUrl: 'app/sections/list/list.html',
        controller: 'ContactListCtrl'
    })
    .when('/edit', {
        templateUrl: 'app/sections/edit/edit.html',
        controller: 'ContactEditCtrl'
    })
    .when('/edit/:id', {
        templateUrl: 'app/sections/edit/edit.html',
        controller: 'ContactEditCtrl'
    })
    .otherwise({
        redirectTo: '/'
    });
});
