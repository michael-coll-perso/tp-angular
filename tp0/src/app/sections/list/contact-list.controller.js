angular.module('zenContact.sections.list', ['zenContact.core.contact'])
.controller('ContactListCtrl', function($scope, contactService) {
    'use strict';
    $scope.contacts = contactService.getAllContacts();
});
