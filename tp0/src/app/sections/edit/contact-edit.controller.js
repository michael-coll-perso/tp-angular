'use strict';

angular.module('zenContact.sections.edit', ['zenContact.core.contact'])
    .controller('ContactEditCtrl', function($scope, $routeParams, contactService) {
        $scope.contact = contactService.getContactById($routeParams.id);
    });
