angular.module('zenContact.sections.nav', [])
    .controller('NavBarCtrl', function($scope, $location) {
        'use strict';
        $scope.isActive = function(path) {
            return $location.path().indexOf(path) !== -1;
        };
    });
